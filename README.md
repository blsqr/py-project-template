# Python Project Template

A template from which to generate a repository for a Python package.

The template integrates a number of tools to make development and maintenance of the Python package easier:

- Default package structure and pre-populated `setup.py`
- Documentation build using [Sphinx][sphinx]
- [Pytest][pytest] environment with basic fixtures
- [Tox][tox] setup to test against different Python versions
- [pre-commit] configuration, including auto-formatting using [black][black]
- Basic [GitLab CI/CD][GitLab-CI-CD] configuration
- [GitLab Issue and MR templates][GitLab-templates]


**Questions or problems with this template?**
Please open an issue here in this repository.


[[_TOC_]]

## Instructions
To make setting up a new Python project as easy as possible, the [cookiecutter](https://github.com/cookiecutter/cookiecutter#features) tool allows to clone a template project (this repository) and make the necessary adjustments on the way.
For instance, the `{{cookiecutter.package_name}}` folder you see in the root of this repository will be named using the `package_name` variable; the same holds for these kinds of variables *inside* any of the files in this project.

*Note:* This template is optimized for hosting the remote on a GitLab instance.
However, it can easily be adapted to GitHub or other hosters; see below.


### Step 1: Install `cookiecutter`
```bash
pip3 install cookiecutter
```


### Step 2: Create your project from the template
☝️**Before proceeding, read the rest of this step carefully!**

First, enter the directory in which your project repository should be created in.

To create your project repository from the template, call the following command:

```bash
cookiecutter https://gitlab.com/blsqr/py-project-template
```

This will prompt for some input from your side.
Let's go through the prompts one by one:

* `author`: your name or that of your team *(may contain spaces)*
* `author_email`: an e-mail address you can be reached by *(optional)*
* `project_name`: the readable name of this project *(may contain spaces)*
* `project_description_short`: a one-sentence description of this project *(optional)*
* `package_name` : The name of your Python package. Should not contain spaces and best follow Python naming conventions.
* `package_name_short` : A short version of the package name. If the name is short anyway, just use the same.
* `package_slug` : A lower-case version of the package or project name, without spaces.
* `version`: The initial version of the package.
* `gitlab_group`: The name of the GitLab group the repo is part of; for your personal repository, this will simply be your GitLab user name. *(optional)*
* `gitlab_repo_slug`: The slug of the repository's remote URL, i.e. the last segment of the `repo_url`. *(optional)*
* `repo_url`: The URL of the repository this project will be hosted at; should *not* be the clone URL. The default value will be composed of `gitlab_group` and `gitlab_repo_slug`.

Now follow the prompts and enter the corresponding information.
Remember that you can always use `Ctrl + C` to stop cookiecutting and start over, e.g. when you have entered wrong information.

After this, cookiecutter will hopefully inform you that it has succeeded.
Your Python project is now basically set up! 🎉

To see more options, call `cookiecutter --help`.
Also refer to the [cookiecutter documentation][cookiecutter_docs] for more information on the available configuration options in this step.


### Step 3: Test that it works
Enter the created directory; it will be named like the `package_name` you entered above.

Follow the instructions in the *created project's* README file to get the tests running, basically:

```bash
cd your-project-directory
pip install -e .[dev]
python -m pytest -v tests/
```



### Step 4: Make adjustments
There are a bunch of adjustments you can now do on your repository to get ready for implementing your Python package.


#### Set up version control *(recommended)*
Your new project does not come with version control yet.
We should definitely change that:

```bash
git init
git remote add origin <THE-PROJECT-REPOSITORY-URL>
```

You probably want a remote host for your git repository.
This template project is specialized for hosting on **GitLab** instances and provides issue and MR templates as well as a CI/CD configuration.
If you want to host there, create a new project now and follow the steps there to connect it to your local repository.

At this point, you can already commit and push if you want, but you can also wait and carry out any further adjustments; it's completely up to you.

For hosting on other platforms, the procedure is basically the same.
In such a case, you can delete the `.gitlab` directory and the `.gitlab-ci.yml` file.

The template also includes some [pre-commit][pre-commit] hooks.
To install the corresponding git hooks, run:

```bash
pre-commit install
```


#### Adjust dependencies *(recommended)*
The `setup.py` file defines some dummy dependencies, which you should adjust.


#### Populate GitLab CI/CD (or delete it)
*Proceed only, if you know what this is and have at least a little bit of experience with it.*

In case you want to use GitLab CI/CD for automated testing, the template project comes with a `.gitlab-ci.yml` to get you started.
If you don't plan on using it, you can safely delete that file.

The CI/CD as configured in the template does the following:

* Run static checks on the code, including code-formatting
* Run tests using different Python versions
* Build and deploy the documentation to GitLab Pages

In very broad strokes, you *may* need to make the following adjustments in `.gitlab-ci.yml` or in the GitLab Project settings:

* Check that the `PAGES_URL` variable is correct and GitLab Pages is activated in the project settings.
* Check that the GitLab Environments feature is enabled.
* Add or adapt the Python test jobs, if you want to support different Python versions (also adjust `tox.ini` accordingly).


## Copyright & License
This project is free open source software, licensed under the [MIT License][MIT].
See [`LICENSE.txt`](LICENSE.txt) for the full text of the license.


<!-- Links -->

[MIT]: https://choosealicense.com/licenses/mit/
[cookiecutter_docs]: http://cookiecutter.readthedocs.io
[pytest]: https://pytest.org/
[black]: https://black.readthedocs.io/
[pre-commit]: https://pre-commit.com
[tox]: https://tox.wiki/
[sphinx]: https://www.sphinx-doc.org
[GitLab-CI-CD]: https://docs.gitlab.com/ee/ci/
[GitLab-templates]: https://docs.gitlab.com/ee/user/project/description_templates.html
