"""Sets up the {{ cookiecutter.package_name }} package installation"""

from setuptools import find_packages, setup

# .. Dependency lists .........................................................

INSTALL_DEPS = [
    "numpy",
    #
    # TODO adjust these 🚧
]

# Dependencies for running tests and general development of utopya
TEST_DEPS = [
    "pytest",
    "pytest-cov",
    "pre-commit",
]

# Dependencies for building the utopya documentation
DOC_DEPS = [
    "sphinx>=5.3,<6",
    "sphinx-book-theme",
    "sphinx-togglebutton",
    "ipython>=7.0",
    "myst-parser[linkify]",
    "sphinx-click",
    "pytest",
]

# .............................................................................


def find_version(*file_paths) -> str:
    """Tries to extract a version from the given path sequence"""
    import codecs
    import os
    import re

    def read(*parts):
        """Reads a file from the given path sequence, relative to this file"""
        here = os.path.abspath(os.path.dirname(__file__))
        with codecs.open(os.path.join(here, *parts), "r") as fp:
            return fp.read()

    # Read the file and match the __version__ string
    file = read(*file_paths)
    match = re.search(r"^__version__\s?=\s?['\"]([^'\"]*)['\"]", file, re.M)
    if match:
        return match.group(1)
    raise RuntimeError("Unable to find version string in " + str(file_paths))


# .............................................................................


setup(
    name="{{ cookiecutter.package_name }}",
    #
    # Package information
    version=find_version("{{ cookiecutter.package_name }}", "__init__.py"),
    #
    description="{{ cookiecutter.project_description_short }}",
    url="{{ cookiecutter.repo_url }}",
    author="{{ cookiecutter.author }}",
    author_email="{{ cookiecutter.author }} <{{ cookiecutter.author_email }}>",
    classifiers=[
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
        #
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        #
        "Topic :: Scientific/Engineering",
    ],
    #
    # Package content
    packages=find_packages(exclude=("tests",)),
    # package_data=dict({{ cookiecutter.package_name }}=["cfg/*.yml"]),
    data_files=[("", ["README.md"])],  # TODO Can add license files here
    #
    # Dependencies
    install_requires=INSTALL_DEPS,
    extras_require=dict(
        test=TEST_DEPS,
        doc=DOC_DEPS,
        dev=TEST_DEPS + DOC_DEPS,
    ),
)
