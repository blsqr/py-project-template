<!-- 1 - Add a description of what this MR does. -->
This MR ...


### Can this MR be accepted?
<!-- 2 - Fill in the checklist below. -->

<!-- Progress: If desired, give a more detailed overview of the progress -->
- [ ] **Implementation** ready
- [ ] **Tests** added or adjusted
- [ ] **Documentation** extended or updated

<!-- Code Quality: Add or ~~striketrough~~ points, if it makes sense -->
- [ ] Code quality
    - [ ] Changes follow coding best practices
    - [ ] Checked code coverage on new and adjusted code

<!-- Criteria for merging -->
- [ ] Ready for merging
    - [ ] Pipeline passes without warnings
    - [ ] History cleaned-up ~~or squash option set~~ <!-- how you prefer -->
    - [ ] [Changelog](CHANGELOG.md) entry added
    - [ ] [Version number]({{cookiecutter.package_name}}/__init__.py) bumped <!-- if applicable -->


### Related issues
<!-- 3 - If applicable, mention related issues here, otherwise delete. -->
Closes #...

<!-- 4 - When ready for review, remove the Draft status & assign a reviewer -->
