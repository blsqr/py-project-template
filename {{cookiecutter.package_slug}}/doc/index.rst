.. _welcome:

``{{cookiecutter.project_name}}``
=========================================================

{{cookiecutter.project_description_short}}

:py:mod:`{{cookiecutter.package_name}}` ... 🚧

.. note::

    If you find any errors in this documentation or would like to contribute to the project, we are happy about your visit to the `project page <{{cookiecutter.repo_url}}>`_.


.. toctree::
    :hidden:

    Repository <{{cookiecutter.repo_url}}>
    Changelog <{{cookiecutter.repo_url}}/-/blob/main/CHANGELOG.md>

.. toctree::
    :caption: Reference
    :maxdepth: 2
    :hidden:

    API Reference <api/{{cookiecutter.package_name}}>
    index_pages
