# {{cookiecutter.project_name}}

{{cookiecutter.project_description_short}}

[Visit the project repository][repository].


## Installation
First, create and/or enter a Python virtual environment in which you would like to install this package.

The package can then be installed using [pip]:

```bash
pip install git+{{cookiecutter.repo_url}}
```



## Use
🚧



## For developers
If you plan on developing for this project, installation should be done from a local `git clone` of the repository.
After having the project cloned, you may want to enter a virtual environment for development.

To then install the package (in editable mode), run:

```
cd {{cookiecutter.package_slug}}
pip install -e .[dev]
```

which will include development-related dependencies (for tests and building of the documentation).

To automatically run [pre-commit][pre-commit] hooks, install the configured git hooks using `pre-commit install`.


### Running tests
Enter the virtual environment, then run [pytest][pytest]:

```bash
python -m pytest -v tests/ --cov={{cookiecutter.package_name}} --cov-report=term-missing
```

### Building the documentation
```bash
cd doc
make doc
make linkcheck  # optional
make doctest    # optional
```

The documentation can then be found in [`doc/_build/html`](doc/_build/html/).

To automatically generate figures, set the `{{ cookiecutter.package_name_short.upper() }}_USE_TEST_OUTPUT_DIR` environment variable before invoking `make doc`.

```bash
export {{ cookiecutter.package_name_short.upper() }}_USE_TEST_OUTPUT_DIR=yes
```


## Copyright
```
{{cookiecutter.project_name}}
(c) {% now 'utc', '%Y' %}, {{cookiecutter.author}}

🚧 <insert license here>
```

### Copyright holders

- {{cookiecutter.author}}


[repository]: {{cookiecutter.repo_url}}
[pip]: https://pip.pypa.io/en/stable/
[pytest]: https://pytest.org/
[pre-commit]: https://pre-commit.com
